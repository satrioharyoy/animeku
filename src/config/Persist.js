import storage from "redux-persist/lib/storage";

const PERSIST = {
  active: true,
  reducerVersion: "1.0",
  storeConfig: {
    key: "root",
    storage,
    whitelist: ["myCollection"],
    transforms: [],
  },
};

export default PERSIST;
