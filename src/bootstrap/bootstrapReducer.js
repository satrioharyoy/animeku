import { combineReducers } from "redux";
import * as STATE from "./bootstrapInitialState";
import * as CONST from "./bootstrapConstant";

// Other reducers
import { collectionReducers } from "@cp-module/my-collection/collectionReducers";
import { homeReducers } from "@cp-module/home/homeReducer";

const bootstrapInitialState = {
  ...STATE.initialState,
  action: "",
};

export const bootstrap = (state = bootstrapInitialState, action) => {
  const { payload, type } = action;
  const actions = {
    // Your Action
    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};

const bootstrapReducers = combineReducers({
  bootstrap,
  myCollection: collectionReducers,
  home: homeReducers,

  // others reducers
});

export default bootstrapReducers;
