import { useRouter } from "next/router";
import { useEffect } from "react";

function Bootstrap({ children }) {
  const router = useRouter();
  useEffect(() => {
    // detect auth

    // redirect from index
    if (router?.pathname === "/") {
      router?.push("/home");
    }
  }, [router]);

  return <>{children}</>;
}

export default Bootstrap;
