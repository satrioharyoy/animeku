import { connect } from "react-redux";
import View from "./View";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(View);
