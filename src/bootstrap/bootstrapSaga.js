import { takeEvery, put, call, all, takeLatest } from "redux-saga/effects";

// Other Sagas
import myCollectionSaga from "@cp-module/my-collection/myCollectionSaga";
import homeSaga from "@cp-module/home/homeSaga";
// Saga Function

const bootstrap = [];

function* bootstrapSagas() {
  yield all([
    ...bootstrap,
    ...homeSaga,
    // ...myCollectionSaga,
    // other sagas
  ]);
}

export default bootstrapSagas;
