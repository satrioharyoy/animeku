import axios from "axios";
import { store } from "@cp-config/Store";
import { setToken, setClearRefreshToken } from "@cp-module/auth/authAction";
import { BASE_URL_API } from "@cp-util/Constant";

export const api = axios.create({
  timeout: 10000,
  baseURL: BASE_URL_API,
  headers: {
    "X-Requested-With": "XMLHttpRequest",
  },
});

api.defaults.headers.common["Accept-Language"] = "en";

api.interceptors.request.use(
  (request) => {
    const token = store.getState().auth.token.access_token;
    request.headers["Content-Type"] = "application/json; charset=UTF-8";
    if (token !== "") {
      request.headers.Authorization = `Bearer ${token}`;
    }
    return request;
  },
  (error) => {
    Promise.reject(error);
  }
);

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    let originalRequest = error.config;
    if (
      error?.response?.status === RESPONSE_STATUS.UNAUTHORIZED &&
      !originalRequest._retry
    ) {
      originalRequest._retry = true;
      const { email } = store.getState().auth.userData;
      const refreshToken = store.getState().auth.token.refresh_token;
      return api
        .post(`${BASE_URL}/v1/auth/refreshToken`, {
          id: email,
          refreshToken,
        })
        .then((res) => {
          if (res?.status === RESPONSE_STATUS.SUCCESS) {
            store.dispatch(
              setToken({
                token: res?.data?.data?.token,
              })
            );
            return api(originalRequest);
          }
          return api(originalRequest);
        })
        .catch(() => {
          setTimeout(() => {
            store.dispatch(setClearRefreshToken());
          }, 1000);
          return null;
        });
    }
    return Promise.reject(error);
  }
);
