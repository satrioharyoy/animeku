import client from "@cp-client";
import { wrapper } from "@cp-config/Store";
import { GET_ANIME_LIST_START } from "@cp-module/detail-anime/homeConstant";
import { getListAnime } from "@cp-module/home/homeAction";
import { ANIME_LIST } from "@cp-module/home/homeApi";
import { GET_ANIME_LIST_INITIAL } from "@cp-module/home/homeConstant";
import { Home } from "@cp-module/home/screen";
import { HYDRATE } from "next-redux-wrapper";

function Page(props) {
  return <Home {...props} />;
}

// export async function getStaticProps() {
//   // Call an external API endpoint to get posts.
//   // You can use any data fetching library
//   const { data, error, networkStatus } = await client.query({
//     query: ANIME_LIST,
//     variables: { page: 1, perPage: 12 },
//   });

//   return {
//     props: {
//       posts: data,
//     },
//   };
// }

export const getServerSideProps = wrapper.getStaticProps(
  (store) =>
    async ({ req, res, ...etc }) => {
      const { data, error, networkStatus } = await client.query({
        query: ANIME_LIST,
        variables: { page: 1, perPage: 12 },
      });
      console.log("cehck data => ", data);
      await store.dispatch({
        type: HYDRATE,
        payload: { getAnimeListResponse: data.Page.media },
      });
    }
);

export default Page;
