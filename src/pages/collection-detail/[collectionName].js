import { CollectionDetail } from "@cp-module/collection-detail/screen";

function Page(props) {
  return <CollectionDetail {...props} />;
}

export default Page;
