import client from "@cp-client";
import { ANIME_DETAIL } from "@cp-module/detail-anime/detailAnimeApi";
import { DetailAnime } from "@cp-module/detail-anime/screen";

function Page(props) {
  return <DetailAnime {...props} />;
}

export async function getServerSideProps(context) {
  // Call an external API endpoint to get posts.
  // You can use any data fetching library
  const id = context.params.animeId;
  const { data, error, networkStatus } = await client.query({
    query: ANIME_DETAIL,
    variables: { id: id },
  });
  return {
    props: {
      anime: data,
    },
  };
}

export default Page;
