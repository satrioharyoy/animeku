import { DefaultSeo } from "next-seo";
import Bootstrap from "@cp-bootstrap/Bootstrap";
import { wrapper } from "@cp-config/Store";
import seo from "../../next-seo.config";
import "@cp-style/_globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <Bootstrap>
      <DefaultSeo {...seo} />
      <Component {...pageProps} />
    </Bootstrap>
  );
}

export default wrapper.withRedux(MyApp);
