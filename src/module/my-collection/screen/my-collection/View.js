import {
  Button,
  Container,
  ModalAddCollection,
  ModalWarning,
  TitleSection,
} from "@cp-component";
import { useForm } from "@cp-util/Hooks/UseForm";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import validate from "../ruleValidation";

function MyCollection(props) {
  const router = useRouter();
  const { collections, setNewCollection, setRemoveCollection } = props;

  const onSubmitNewCollection = (value, resetValue) => {
    setNewCollection(value.collectionName);
    resetValue();
  };

  const [modal, setModal] = useState(false);
  const [idOnRemove, setIdOnRemove] = useState(null);
  const [form, setForm, onSubmitForm, errors, isSubmitting, resetValue] =
    useForm(
      {
        collectionName: "",
      },
      validate,
      onSubmitNewCollection
    );

  const onClickRemoveButton = (id) => {
    setIdOnRemove(id);
    setModal({ modalWarningDelete: true });
  };

  const onRemoveCollection = () => {
    setModal(false);
    setRemoveCollection(idOnRemove);
  };

  const onClickAddCollection = () => {
    setModal(false);
    onSubmitForm(form);
  };

  const onClickCloseModal = () => {
    setModal(false);
    resetValue();
  };

  const getCollectionNameAnimes = (animes) => {
    let animesName = animes.map((item) => {
      return item.title.english;
    });
    animesName = animesName.join(", ");
    return animesName;
  };

  return (
    <>
      <Container>
        <div className="py-24 lg:px-32 flex-1 md:px-12 sm:px-12">
          <TitleSection title={"My Collection"} />
          <div className="flex flex-row justify-end">
            <Button
              title={"Add New Collection"}
              className="w-52"
              onClick={() => setModal({ modalAddCollection: true })}
            />
          </div>
          <ul>
            {collections && collections.length > 0
              ? collections.map((item) => {
                  return (
                    <li
                      key={item.name}
                      className="flex w-full rounded-lg bg-background-secondary mt-3 min-h-32 p-5 flex-row"
                    >
                      <Image
                        width={200}
                        height={300}
                        objectFit="cover"
                        objectPosition="center"
                        src={
                          item?.animes[0]?.coverImage.large ??
                          "https://s4.anilist.co/file/anilistcdn/media/anime/cover/large/bx1-CXtrrkMpJ8Zq.png"
                        }
                      />
                      <article className="ml-10">
                        <h3 className="text-typhoColor-primary">{item.name}</h3>
                        <p className="text-typhoColor-primary">
                          {getCollectionNameAnimes(item.animes)}
                        </p>
                        <div className="flex flex-row gap-10">
                          <Button
                            title={"Open this collection"}
                            onClick={() =>
                              router.push(`/collection-detail/${item.name}`)
                            }
                          />
                          <Button
                            title={"Remove this collection"}
                            onClick={() => onClickRemoveButton(item.name)}
                          />
                        </div>
                      </article>
                    </li>
                  );
                })
              : null}
          </ul>
        </div>
        {modal.modalWarningDelete && (
          <ModalWarning
            labelButtonCancel="Cancel"
            labelButtonConfirm="Yes"
            title="Are You Sure Want to remove this ?"
            description="if you press yes, the data will be deleted forever"
            onClickCancel={() => setModal(false)}
            onClickConfirm={onRemoveCollection}
          />
        )}
        {modal.modalAddCollection && (
          <ModalAddCollection
            title="Add New Collection"
            labelButtonCancel="Cancel"
            labelButtonConfirm="Add collection"
            valueCollection={form.collectionName}
            onChangeInputText={(event) =>
              setForm("collectionName", event.target.value)
            }
            onClickCancel={onClickCloseModal}
            errorInput={Object.keys(errors).length > 0 ? true : false}
            errorMessage={errors.collectionName}
            onClickConfirm={onClickAddCollection}
            type="addCollection"
          />
        )}
      </Container>
    </>
  );
}

export default MyCollection;
