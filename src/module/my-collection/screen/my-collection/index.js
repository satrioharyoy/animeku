import { connect } from "react-redux";
import {
  setNewCollection,
  setRemoveCollection,
} from "@cp-module/my-collection/myCollectionAction";
import MyCollection from "./View";

const mapStateToProps = (state) => ({
  collections: state.myCollection.collections,
});

const mapDispatchToProps = {
  setNewCollection: (payload) => setNewCollection(payload),
  setRemoveCollection: (payload) => setRemoveCollection(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(MyCollection);
