export default function validate(values) {
  let errors = {};
  const cantEmptyMessage = "Field Cant be Empty";
  const characterCheck = /^[A-Za-z0-9]+$/;

  if (!values.collectionName) {
    errors.collectionName = cantEmptyMessage;
  }
  if (!characterCheck.test(values.collectionName)) {
    errors.collectionName = "special characters are not allowed";
  }
  return errors;
}
