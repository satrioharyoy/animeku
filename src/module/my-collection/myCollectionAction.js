import * as CONST from "@cp-module/my-collection/myCollectionConstant";

export const setNewCollection = (payload) => ({
  type: CONST.SET_ADD_NEW_COLLECTION,
  payload: {
    name: payload,
    animes: [],
  },
});

export const setRemoveCollection = (payload) => ({
  type: CONST.SET_REMOVE_COLLECTION,
  payload,
});

export const setAnimeCollection = (payload) => ({
  type: CONST.SET_ANIME_TO_COLLECTION,
  payload,
});

export const setRemoveAnimefromCollection = (payload) => ({
  type: CONST.SET_REMOVE_ANIME_FROM_COLLECTION,
  payload,
});
