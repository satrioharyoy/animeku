import * as CONST from "@cp-module/my-collection/myCollectionConstant";
import * as STATE from "@cp-module/my-collection/myCollectionInitialState";

const myCollectionInitialState = {
  ...STATE.collectionInitialState,
  action: "",
};

const setAnimetoCollection = (collections, collectionChoosed, dataAnime) => {
  let newCollections = collections.map((obj) => {
    if (collectionChoosed.some((item) => item.id === obj.name)) {
      const found = obj.animes.some((el) => el.id === dataAnime.id);
      if (!found) {
        return { ...obj, animes: [...obj.animes, dataAnime] };
      }
    }
    return obj;
  });
  return newCollections;
};

const setRemoveAnimefromCollection = (collectionName, animeId, collections) => {
  let dataCollections = collections;
  const collectionIndex = dataCollections.findIndex(
    (item) => item.name === collectionName
  );
  const newCollectionsAnimes = dataCollections[collectionIndex].animes.filter(
    (item) => item.id !== animeId
  );
  dataCollections[collectionIndex].animes = newCollectionsAnimes;
  return dataCollections;
};

export const collectionReducers = (
  state = myCollectionInitialState,
  action
) => {
  const { payload, type } = action;
  const actions = {
    //Get All Menu
    [CONST.SET_ADD_NEW_COLLECTION]: () => ({
      ...state,
      collections:
        state.collections.length > 0 &&
        state.collections.find((obj) => obj.name === payload.name)
          ? [...state.collections]
          : state.collections.concat({ ...payload }),
      action: type,
    }),
    [CONST.SET_REMOVE_COLLECTION]: () => ({
      ...state,
      collections: state.collections.filter(
        (collection) => collection.name != payload
      ),
      action: type,
    }),
    [CONST.SET_ANIME_TO_COLLECTION]: () => ({
      ...state,
      collections: setAnimetoCollection(
        state.collections,
        payload.selectedCollection,
        payload.dataAnime
      ),
    }),
    [CONST.SET_REMOVE_ANIME_FROM_COLLECTION]: () => ({
      ...state,
      collections: setRemoveAnimefromCollection(
        payload.collectionName,
        payload.animeId,
        state.collections
      ),
      action: type,
    }),
    DEFAULT: () => state,
  };

  return (actions[type] || actions.DEFAULT)();
};
