import {
  setAnimeCollection,
  setNewCollection,
  setRemoveAnimefromCollection,
} from "@cp-module/my-collection/myCollectionAction";
import { connect } from "react-redux";
import CollectionDetail from "./View";

const mapStateToProps = (state) => ({
  collections: state.myCollection.collections,
});

const mapDispatchToProps = {
  setAnimeCollection: (payload) => setAnimeCollection(payload),
  setNewCollection: (payload) => setNewCollection(payload),
  setRemoveAnimefromCollection: (payload) =>
    setRemoveAnimefromCollection(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(CollectionDetail);
