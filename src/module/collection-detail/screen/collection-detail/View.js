import { Button, Container, ModalWarning, TitleSection } from "@cp-component";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";

function CollectionDetail({ collections, setRemoveAnimefromCollection }) {
  const router = useRouter();
  const { collectionName } = router.query;
  const data = collections?.filter((item) => item.name === collectionName);
  const [openModalRemove, setOpenModalRemove] = useState(false);
  const [onSelectAnimeId, setOnSelectAnimeId] = useState(null);

  const onRemoveAnime = () => {
    setRemoveAnimefromCollection({ collectionName, animeId: onSelectAnimeId });
    setOpenModalRemove(false);
  };

  const onOpenRemoveModal = (animeId) => {
    setOnSelectAnimeId(animeId);
    setOpenModalRemove(true);
  };

  return (
    <>
      <Container>
        <div className="py-24 lg:px-32 flex-1 md:px-12 sm:px-12">
          <TitleSection title={data[0]?.name ?? ""} />
          <ul>
            {data[0]?.animes
              ? data[0].animes.map((item) => {
                  return (
                    <li
                      key={item.id}
                      className="flex w-full rounded-lg bg-background-secondary mt-3 min-h-32 p-5 flex-row"
                    >
                      <Image
                        width={150}
                        height={150}
                        objectFit="cover"
                        objectPosition="center"
                        src={
                          item?.coverImage.large ??
                          "https://s4.anilist.co/file/anilistcdn/media/anime/cover/large/bx1-CXtrrkMpJ8Zq.png"
                        }
                        className="rounded-md"
                      />
                      <section className="ml-10">
                        <h3 className="text-typhoColor-primary">
                          {item.title.english}
                        </h3>
                        <div className="flex flex-row gap-10">
                          <Button
                            title={"Open Anime Detail"}
                            onClick={() =>
                              router.push(`/detail-anime/${item.id}`)
                            }
                          />
                          <Button
                            title={"Remove this collection"}
                            onClick={() => onOpenRemoveModal(item.id)}
                          />
                        </div>
                      </section>
                    </li>
                  );
                })
              : null}
          </ul>
        </div>
        {openModalRemove && (
          <ModalWarning
            labelButtonCancel="Cancel"
            labelButtonConfirm="Yes"
            title="Are You Sure Want to remove this ?"
            description="if you press yes, the data will be deleted forever"
            onClickCancel={() => setOpenModalRemove(false)}
            onClickConfirm={onRemoveAnime}
          />
        )}
      </Container>
    </>
  );
}

export default CollectionDetail;
