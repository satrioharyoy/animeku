import { gql } from "@apollo/client";

export const ANIME_DETAIL = gql`
  query GetDetailAnime($id: Int) {
    Media(id: $id) {
      id
      title {
        english
        native
      }
      format
      description
      duration
      season
      averageScore
      source
      episodes
      coverImage {
        extraLarge
        large
        medium
        color
      }
      bannerImage
      studios {
        nodes {
          name
        }
      }
      genres
    }
  }
`;
