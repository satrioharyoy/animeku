import { Container, ModalAddCollection, TitleSection } from "@cp-component";
import { useMediaQuery } from "@cp-util/Hooks/MediaQuery";
import Image from "next/image";
import { useRouter } from "next/router";
import { useMemo, useState } from "react";
import styles from "../../screen/styles.module.css";
import dynamic from "next/dynamic";

const Button = dynamic(() =>
  import("../../../../component/atoms/Button", {
    ssr: false,
  })
);

function DetailAnime({
  anime,
  collections,
  setAnimeCollection,
  setNewCollection,
}) {
  const dataAnime = anime?.Media ?? {};
  const router = useRouter();
  let isWebWide = useMediaQuery("(min-width: 1200px)");
  const animeGenres = dataAnime?.genres?.join(", ") ?? "Unknown";
  const [modal, setModal] = useState(false);
  const [selectedCollection, setSelectedCollection] = useState([]);
  const [newCollection, setOnNewCollection] = useState("");
  const collectionInfo = collections.map((item) => {
    if (item.animes.length > 0) {
      let onCollection = item.animes.some(
        (element) => element.id === dataAnime.id
      );
      if (onCollection) {
        return item.name;
      }
    }
  });

  const getCollectionInfo = useMemo(
    () => collectionInfo,
    [dataAnime, collections]
  );

  const getStudiosName = (studios) => {
    let studiosName = studios.map((item) => {
      let array = [];
      array.push(item.name);
      return array;
    });
    studiosName = studiosName.join(", ");
    return studiosName;
  };

  const getListItemOptions = (item) => {
    let listItem = item.map((item) => {
      return { label: item.name, id: item.name };
    });
    return listItem;
  };

  const onSelectedCollection = (item) => {
    const isExist = selectedCollection.some((value) => value.id === item.id);
    if (!isExist) {
      setSelectedCollection(selectedCollection.concat(item));
    } else {
      return null;
    }
  };

  const onRemoveSelectedCollection = (item) => {
    const filtered = selectedCollection.filter((e) => e.name !== item);
    setSelectedCollection(filtered);
  };

  const onSubmitAddAnime = () => {
    if (collections.length > 0) {
      setAnimeCollection({
        selectedCollection,
        dataAnime: {
          id: dataAnime.id,
          coverImage: dataAnime.coverImage,
          title: dataAnime.title,
        },
      });
      setModal(false);
    } else {
      setNewCollection(newCollection);
    }
  };

  return (
    <>
      <Container>
        <div className="flex flex-col">
          <aside className="w-full">
            <Image
              src={dataAnime?.bannerImage}
              alt="one piece"
              objectFit={"cover"}
              objectPosition="center"
              width="1500"
              height="300"
              quality={100}
            />
          </aside>
          <div className="mt-6 lg:px-40 flex-1 md:px-12 sm:px-12 xs:px-12">
            <div className="flex xl:flex-row lg:flex-row sm:flex-col md:flex-col xs:flex-col gap-6">
              <section className="flex lg:w-1/3 md:w-full sm:w-full xs:w-full flex-col">
                <div className="flex relative items-center justify-center">
                  <Image
                    src={dataAnime?.coverImage.extraLarge}
                    alt="one piece"
                    objectFit={isWebWide ? "cover" : "contain"}
                    objectPosition={isWebWide ? "center" : null}
                    width="300"
                    height="400"
                    className="rounded-lg"
                    quality={100}
                  />
                  <div className="text-typhoColor-primary bg-background-secondary h-8 items-center px-2 top-0 right-0 absolute opacity-70 text-left text-xl bottom-0">
                    {dataAnime?.averageScore} / 100
                  </div>
                </div>
                <Button
                  title="Add to collection"
                  onClick={() => setModal(true)}
                />
              </section>
              <article className="w-3/4 xs:w-full sm:w-full md:w-full bg-background-secondary p-4 xs:p-7 sm:p-7 md:p-7  rounded-md">
                <h1 className="text-typhoColor-primary font-bold text-xl">
                  {dataAnime.title?.english ?? dataAnime?.title?.native}
                </h1>
                <p className="text-typhoColor-secondary mt-3 text-sm text-justify">
                  {dataAnime.description}
                </p>
                <section>
                  <h2 className="text-typhoColor-primary font-bold text-xl mt-5">
                    Collection Info
                  </h2>
                  {collectionInfo.length > 0 &&
                    collectionInfo[0] !== undefined &&
                    collectionInfo.map((item) => {
                      return (
                        <Button
                          key={item}
                          title={item}
                          onClick={() =>
                            router.push(`/collection-detail/${item}`)
                          }
                        />
                      );
                    })}
                </section>
              </article>
            </div>
            <section className="mt-10">
              <TitleSection title={"Anime Detail"} />
              <div className="flex flex-row xs:flex-col sm:flex-col py-2 bg-background-secondary mt-3">
                <table className="text-typhoColor-secondary text-left">
                  <tbody>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        Japanese
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>{`:  ${dataAnime?.title?.native ?? "Unknown"}`}</p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        English
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>{`:  ${dataAnime?.title?.english ?? "Unknown"}`}</p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        Type
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>{`:  ${dataAnime?.format ?? "Unknown"}`}</p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        Duration
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>{`:  ${dataAnime?.duration ?? "Unknown"} min.`}</p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        Season
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>{`:  ${dataAnime?.season ?? "Unknown"}`}</p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        Episodes
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>{`:  ${dataAnime?.episodes ?? "Unknown"}`}</p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        Genres
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>
                          {`:
                           ${
                             dataAnime?.genres.length > 0
                               ? animeGenres
                               : "Unknown"
                           }`}
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <th colSpan={1} className={styles.th}>
                        Producers
                      </th>
                      <td colSpan={2} className={styles.td}>
                        <p>
                          {`:  ${
                            dataAnime?.studios?.nodes?.length > 0
                              ? getStudiosName(dataAnime.studios.nodes)
                              : "Unknown"
                          }`}
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </section>
          </div>
        </div>
      </Container>
      {modal && (
        <ModalAddCollection
          title="Add to collection"
          labelButtonCancel="Cancel"
          labelButtonConfirm="Yes"
          onClickCancel={() => setModal(false)}
          collectionList={getListItemOptions(collections)}
          selectedCollection={selectedCollection}
          onSelectCollection={onSelectedCollection}
          onRemoveSelectedCollection={onRemoveSelectedCollection}
          onClickConfirm={onSubmitAddAnime}
          valueCollection={newCollection}
          onChangeInputText={(e) => setOnNewCollection(e.target.value)}
        />
      )}
    </>
  );
}

export default DetailAnime;
