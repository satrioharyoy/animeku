import {
  setAnimeCollection,
  setNewCollection,
} from "@cp-module/my-collection/myCollectionAction";
import { connect } from "react-redux";
import DetailAnime from "./View";

const mapStateToProps = (state) => ({
  collections: state.myCollection.collections,
});

const mapDispatchToProps = {
  setAnimeCollection: (payload) => setAnimeCollection(payload),
  setNewCollection: (payload) => setNewCollection(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailAnime);
