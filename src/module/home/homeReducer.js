import * as CONST from "./homeConstant";
import * as STATE from "./homeInitialState";
import { HYDRATE } from "next-redux-wrapper";
import { diff } from "jsondiffpatch";

const myCollectionInitialState = {
  ...STATE.homeInitialState,
  action: "",
};

export const homeReducers = (state = myCollectionInitialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case HYDRATE:
      let nextState = {
        ...state,
        ...payload, // apply delta from hydration
      };
      if (state.home?.getAnimeListResponse) {
        nextState.getAnimeListResponse = state.home.getAnimeListResponse;
        delete nextState.home; // preserve count value on client side navigation
      }
      return nextState;
    default:
      return state;
  }
};
// const actions = {
//   //Get All Menu
//   [HYDRATE]: () => ({
//     ...(payload?.home
//       ? payload.getAnimeListResponse
//       : payload.getAnimeListResponse),
//     ...state,
//   }),
//   [CONST.GET_ANIME_LIST_START]: () => ({
//     ...state,
//     getAnimeListFetch: true,
//     getAnimeListParam: payload ?? [],
//     getAnimeListError: false,
//     action: type,
//   }),
//   [CONST.GET_ANIME_LIST_SUCCESS]: () => ({
//     ...state,
//     getAnimeListFetch: false,
//     getAnimeListResponse: payload,
//     action: type,
//   }),
//   [CONST.GET_ANIME_LIST_FAILED]: () => ({
//     ...state,
//     getAnimeListError: true,
//     getAnimeListFetch: false,
//     action: type,
//   }),
//   DEFAULT: () => state,
// };

// return (actions[type] || actions.DEFAULT)();
