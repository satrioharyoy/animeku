export const homeInitialState = {
  getAnimeListFetch: false,
  getAnimeListParam: null,
  getAnimeListResponse: [],
  getAnimeListError: null,
};
