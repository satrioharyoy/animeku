import * as CONST from "./homeConstant";

export const getListAnimeInitial = (payload) => ({
  type: CONST.GET_ANIME_LIST_INITIAL,
  payload,
});

export const getListAnime = (payload) => ({
  type: CONST.GET_ANIME_LIST_START,
  payload,
});

export const getListAnimeSucces = (payload) => ({
  type: CONST.GET_ANIME_LIST_SUCCESS,
  payload,
});

export const getListAnimeFailed = (payload) => ({
  type: CONST.GET_ANIME_LIST_FAILED,
  payload,
});
