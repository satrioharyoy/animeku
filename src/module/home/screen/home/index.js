import { connect } from "react-redux";
import { setNewCollection } from "@cp-module/my-collection/myCollectionAction";
import View from "./view";

const mapStateToProps = (state) => ({
  collections: state.myCollection.collections,
  getAnimeListResponse: state.home.getAnimeListResponse,
  state,
});

const mapDispatchToProps = {
  setNewCollection: (payload) => setNewCollection(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(View);
