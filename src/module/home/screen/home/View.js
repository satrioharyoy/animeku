import { CardList, Container, TitleSection } from "@cp-component";
import { useRouter } from "next/router";
import { useState } from "react";
import { useSelector } from "react-redux";

function home(props) {
  const router = useRouter();
  const dataAnime = useSelector((state) => state.home.getAnimeListResponse);

  return (
    <Container>
      <div className="py-24 lg:px-32 flex-1 md:px-12 sm:px-12">
        <TitleSection title={"Anime List"} />
        <div className="mt-5 grid lg:grid-cols-6 grid-row-2 row-end-5 justify-between gap-5 md:grid-cols-3 sm:grid-cols-3">
          {dataAnime &&
            dataAnime.map((item) => {
              return (
                <CardList
                  key={item.id}
                  title={item?.title?.english ?? item?.title?.native}
                  description="The elite members of Shuchiin Academy's student council continue their competitive day-to-day antics. Council president Miyuki Shirogane clashes daily against vice-president Kaguya Shinomiya, each fighting tooth and nail to trick the other into confessing their romantic love. Kaguya struggles within the strict confines of her wealthy, uptight family, rebelling against her cold default demeanor as she warms to Shirogane and the rest of her friends.

              Meanwhile, council treasurer Yuu Ishigami suffers under the weight of his hopeless crush on Tsubame Koyasu, a popular upperclassman who helps to instill a new confidence in him. Miko Iino, the newest student council member, grows closer to the rule-breaking Ishigami while striving to overcome her own authoritarian moral code.

              As love further blooms at Shuchiin Academy, the student council officers drag their outsider friends into increasingly comedic conflicts.

              (Source: MAL Rewrite)

              Notes:
              - The 10-minute-long trailer is not added to the site, but the staff for it have been credited in this entry.
              - The first episode had an advanced screening on April 2, in both New York & Los Angeles.
              - Episode 12 is a double length episode."
                  image={item?.coverImage?.large ?? ""}
                  date={`${item?.startDate?.day}-${item?.startDate?.month}-${item?.startDate?.year}`}
                  onClick={() => router.push(`detail-anime/${item.id}`)}
                />
              );
            })}
        </div>
      </div>
    </Container>
  );
}

export default home;
