import client from "@cp-client";
import { ANIME_LIST } from "@cp-module/my-collection/homeApi";
import { HYDRATE } from "next-redux-wrapper";
import { useDispatch } from "react-redux";
import { put, takeLatest } from "redux-saga/effects";
import {
  getListAnimeFailed,
  getListAnimeSucces,
  getListAnimeInitial,
} from "./homeAction";
import { GET_ANIME_LIST_INITIAL, GET_ANIME_LIST_START } from "./homeConstant";

function* setAnimeListTask(params) {
  const { data, error, networkStatus } = yield client.query({
    query: ANIME_LIST,
    variables: params.payload,
  });
  if (data) {
    yield put(getListAnimeSucces(data.media));
  }
  if (error) {
    yield put(getListAnimeFailed());
  }
}

// function* setAnimeListInitialTask(params) {
//   const dispatch = useDispatch();
//   const { data, error, networkStatus } = yield client.query({
//     query: ANIME_LIST,
//     variables: params.payload,
//   });

//   yield put(dispatch({ type: HYDRATE, payload: data }));
//   if (error) {
//     yield put(getListAnimeFailed());
//   }
// }

export default [
  takeLatest(GET_ANIME_LIST_START, setAnimeListTask),
  // takeLatest(GET_ANIME_LIST_INITIAL, setAnimeListInitialTask),
];
