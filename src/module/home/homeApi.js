import { gql } from "@apollo/client";

export const ANIME_LIST = gql`
  query GetAnimeList($page: Int, $perPage: Int) {
    Page(page: $page, perPage: $perPage) {
      pageInfo {
        total
        currentPage
        lastPage
        hasNextPage
        perPage
      }
      media(type: ANIME, sort: FAVOURITES_DESC) {
        id
        title {
          english
          native
        }
        coverImage {
          large
        }
        duration
        startDate {
          year
          month
          day
        }
      }
    }
  }
`;
