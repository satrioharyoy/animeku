function TitleSection({ title }) {
  return (
    <div className="bg-background-secondary p-2 w-full border-l-8 border-background-tertiary">
      <h2 className="text-white text-xl ml-3">{title}</h2>
    </div>
  );
}

export default TitleSection;
