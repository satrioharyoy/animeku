import cx from "classnames";
import { useState } from "react";
import styles from "./styles.module.css";

function InputText({
  id,
  type = "text",
  placeholder,
  value,
  disabled = false,
  name,
  onChange,
  className,
  labelName,
  error,
  errorText,
}) {
  const [inputType, setInputType] = useState(type);
  const [isFocus, setFocus] = useState(false);
  const [visible, setVisible] = useState(false);

  return (
    <>
      <h3 className="font-semibold mb-3">{labelName}</h3>
      <div
        className={cx(
          "relative px-4 border-2 rounded-md",
          isFocus || (value && placeholder)
            ? `pt-6 pb-2 border-neutral-40 ${
                error ? "border-error-40" : "border-neutral-20"
              }`
            : `py-4 ${error ? "border-error-40" : "border-neutral-20"}`,
          className,
          disabled ? "bg-disableColor-primary" : "bg-white",
          errorText ? "mb-2" : "mb-6"
        )}
      >
        <>
          {isFocus || value ? (
            <label className="absolute z-0 top-1 text-neutral-20 text-xs font-medium">
              {placeholder}
            </label>
          ) : null}
        </>
        <div className="flex gap-3">
          <input
            id={id}
            disabled={disabled}
            name={name}
            className={`${styles.inputStyle} ${
              disabled ? "" : "text-neutral-90"
            }`}
            type={inputType}
            placeholder={placeholder}
            value={value}
            onChange={onChange}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
          />
        </div>
      </div>
      {errorText ? (
        <p className="mb-4 text-sm text-error-40 ml-2">{errorText}</p>
      ) : null}
    </>
  );
}

export default InputText;
