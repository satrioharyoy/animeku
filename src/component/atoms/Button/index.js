import cx from "classnames";

function Button({ className, title, onClick, disabled }) {
  return (
    <button
      title="Add to collection"
      className={cx(
        "bg-background-tertiary w-full text-typhoColor-secondary h-12 mt-4 rounded-md",
        disabled ? "bg-gray-500" : null,
        className
      )}
      onClick={onClick}
      disabled={disabled}
    >
      {title}
    </button>
  );
}
export default Button;
