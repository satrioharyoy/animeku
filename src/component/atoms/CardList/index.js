import Image from "next/image";

function CardList({ title, description, date, image, onClick }) {
  return (
    <div
      onClick={onClick}
      className="group flex justify-center text-center relative cursor-pointer"
    >
      <Image src={image} height={500} width={350} />
      <article className="absolute h-28 w-full bg-background-tertiary overflow-y-auto opacity-70 text-left bottom-0 font-semibold px-3 py-2 group-hover:ease-in group-hover:h-full group-hover:duration-500">
        <h3>{title}</h3>
        <p className="mt-1">{date}</p>
        <p className="p-1 hidden group-hover:block  bg-background-secondary text-white text-md">
          {description.length > 100
            ? `${description.substring(0, 100)}...`
            : description}
        </p>
        <p className="float-right px-2 hidden group-hover:block mt-2 bg-background-secondary text-white self-end text-md">
          Read More
        </p>
      </article>
    </div>
  );
}

export default CardList;
