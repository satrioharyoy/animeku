import Header from "../../molecules/Header";
import Footer from "../../molecules/Footer";
import { useMediaQuery } from "@cp-util/Hooks/MediaQuery";
import styles from "./styles.module.css";
import Button from "src/component/atoms/Button";
import { useState } from "react";
import { useRouter } from "next/router";
import SideBar from "src/component/molecules/SideBar";
import { IconMenu } from "@cp-config/Images";
import Image from "next/image";

function Component({ children }) {
  let isWebWide = useMediaQuery("(min-width: 1000px)");
  const [sideBar, setSideBar] = useState(false);
  const router = useRouter();

  const menuList = [
    {
      id: "home",
      title: "Home",
      slug: "home",
      // icon: (
      //   <Image src={IconMenu} height={30} width={30} className="bg-white" />
      // ),
    },
    {
      id: "my-collection",
      title: "My Collection",
      slug: "my-collection",
    },
  ];

  return (
    <div className="bg-black h-full w-full">
      <div className="justify-between">
        {isWebWide && <Header menuList={menuList} />}
        <main className="mb-32 ">
          {children}
          {!isWebWide && !sideBar && (
            <div className="fixed bottom-0 w-full">
              <Button
                className={`fi fi-rr-apps ${styles.floatButton}`}
                onClick={() => setSideBar(!sideBar)}
              />
            </div>
          )}
        </main>
        <Footer />
        {sideBar && (
          <SideBar
            menuList={menuList}
            onCloseSideBar={() => setSideBar(!sideBar)}
          />
        )}
      </div>
    </div>
  );
}

export default Component;
