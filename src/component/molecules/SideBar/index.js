import { useRouter } from "next/router";
import Button from "src/component/atoms/Button";

export default function SideBar({ menuList, onCloseSideBar }) {
  const router = useRouter();

  return (
    <aside
      className="w-64 bottom-0 right-0 h-full fixed ease-in-out bg-background-secondary duration-750"
      aria-label="Sidebar"
    >
      <div className="overflow-y-auto h-full  py-4 px-3  rounded ">
        <Button
          className="text-left px-2 bg-background-secondary"
          onClick={onCloseSideBar}
          title="<< Back"
        />
        <ul className="space-y-2">
          {menuList &&
            menuList.map((item) => {
              return (
                <li key={item.id}>
                  <button
                    onClick={() => router.push(`/${item.slug}`)}
                    className="flex text-typhoColor-primary items-center p-2 text-base w-full font-normal rounded-lg  hover:bg-gray-100 dark:hover:bg-gray-700"
                  >
                    {item.icon}
                    <span className="ml-3">{item.title}</span>
                  </button>
                </li>
              );
            })}
        </ul>
      </div>
    </aside>
  );
}
