import Button from "src/component/atoms/Button";

const ModalWarning = ({
  title,
  description,
  labelButtonCancel,
  labelButtonConfirm,
  onClickCancel,
  onClickConfirm,
  type = "warning",
}) => {
  return (
    <div
      className="relative z-30"
      aria-labelledby="modal-title"
      role="dialog"
      aria-modal="true"
    >
      {/* <!-- 
  Background backdrop, show/hide based on modal state. 
 
  Entering: "ease-out duration-300" 
    From: "opacity-0" 
    To: "opacity-100" 
  Leaving: "ease-in duration-200" 
    From: "opacity-100" 
    To: "opacity-0" 
--> */}
      <div className="fixed inset-0 bg-background-primary bg-opacity-75 transition-opacity"></div>

      <div className="fixed z-10 inset-0 overflow-y-auto">
        <div className="flex items-end sm:items-center justify-center min-h-full p-4 text-center sm:p-0">
          {/* <!-- 
      Modal panel, show/hide based on modal state. 
 
      Entering: "ease-out duration-300" 
        From: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" 
        To: "opacity-100 translate-y-0 sm:scale-100" 
      Leaving: "ease-in duration-200" 
        From: "opacity-100 translate-y-0 sm:scale-100" 
        To: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" 
    --> */}
          <div className="relative bg-background-secondary rounded-lg text-left overflow-hidden shadow-xl transform transition-all m-auto w-1/2">
            <div className="bg-background-secondary pt-5 pb-4 sm:px-1 sm:py-3 sm:pb-4">
              <div className="sm:flex sm:items-start">
                <div className="sm:px-4 sm:text-left w-full">
                  <div className="flex flex-row justify-between">
                    <h3
                      className="text-lg leading-6 text-typhoColor-primary font-semibold"
                      id="modal-title"
                    >
                      {title}
                    </h3>
                    <button
                      onClick={onClickCancel}
                      className="h-0 flex-end text-xl text-neutral-20"
                    >
                      x
                    </button>
                  </div>
                  <hr className="mt-4 border-1 border-solid border-borderColor-primary w-full" />
                  <div className="mt-2">
                    <p className="text-sm font-medium text-neutral-20">
                      {description}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            {type === "warning" && (
              <div className="flex flex-row justify-between my-5 mx-4">
                <Button
                  className="h-auto py-1 px-1 w-1/2 bg-background-secondary border border-background-tertiary mr-3"
                  title={labelButtonCancel}
                  classNameLabel="text-xs text-neutral-60"
                  onClick={onClickCancel}
                />
                <Button
                  className="h-1/5 py-1 px-1 w-1/2 ml-3"
                  title={labelButtonConfirm}
                  classNameLabel="text-xs"
                  onClick={onClickConfirm}
                />
              </div>
            )}
            {type === "alert" && (
              <div className="flex my-5 mx-4">
                <Button
                  label={labelButtonConfirm}
                  classNameLabel="text-xs"
                  handleOnClick={onClickConfirm}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalWarning;
