import styles from "./styles.module.css";

function FooterComponent() {
  return (
    <footer className={styles.container}>
      <section className={styles.infoContainer}>
        <article className={styles.articleSection}>
          <h2 className={styles.title}>Animeku</h2>
          <p className={styles.descText}>
            Animeku is a web application that displays anime in the world and
            your favorite anime list.
          </p>
        </article>
        <aside className={`${styles.articleSection} lg:ml-4 md:ml-4`}>
          <div>
            <h2 className={styles.title}>Links</h2>
            <nav className={styles.descText}>About Us</nav>
          </div>
          <h2 className={styles.title}>Social Media</h2>
          <nav className={styles.descText}>Instagram</nav>
          <nav className={styles.descText}>Twitter</nav>
        </aside>
        <article className={styles.articleSection}>
          <h2 className={styles.title}>Have a question ?</h2>
          <p className={styles.descText}>Phone : +62 838 2321 0947</p>
          <p className={styles.descText}>Email : support_animeku@gmail.com</p>
          <p className={styles.descText}>
            Alamat : Jl. Mardani I Cemp. Putih Bar., Kec. Cemp. Putih, Kota
            Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10520
          </p>
        </article>
      </section>
      <section>
        <p className={styles.copyrightText}>Copyright &copy; 2022 Animeku</p>
      </section>
    </footer>
  );
}

export default FooterComponent;
