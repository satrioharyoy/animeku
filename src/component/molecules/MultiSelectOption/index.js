import Icon from "react-icons-kit";
import { angleDown } from "react-icons-kit/fa/angleDown";
import { angleUp } from "react-icons-kit/fa/angleUp";
import { remove } from "react-icons-kit/fa/remove";
import cx from "classnames";
import { useState } from "react";

const Dropdown = ({ list, addItem }) => {
  return (
    <div
      id="dropdown"
      className="absolute z-10 shadow top-100 bg-white rounded-lg h-28 w-1/3 overflow-y-auto "
    >
      <div className="flex flex-col w-full">
        {list.map((item, key) => {
          return (
            <div
              key={key}
              className="cursor-pointer w-full border-gray-100 rounded-t border-b hover:bg-background-tertiary"
              onClick={() => addItem({ id: item.id, label: item.label })}
            >
              <div className="flex w-full items-center p-2 pl-2 border-transparent border-l-2 relative hover:border-teal-100">
                <div className="w-full items-center flex">
                  <div className="mx-2 leading-6">{item.label}</div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

const Multiselect = ({
  items,
  onClickSelectItem,
  onClickRemoveItem,
  selectedItems,
  placeholder,
  labelName,
}) => {
  // state showing if dropdown is open or closed
  const [dropdown, setDropdown] = useState(false);

  const toogleDropdown = () => {
    setDropdown(!dropdown);
  };

  const handleAddNewItem = (item) => {
    onClickSelectItem(item);
    setDropdown(false);
  };

  return (
    <div className="autcomplete-wrapper">
      <div className="autcomplete">
        <div className="w-full flex flex-col items-center mx-auto">
          <div className="w-full">
            <div className="flex flex-col items-center relative">
              <div className="w-full ">
                <p className="font-semibold mb-3">{labelName}</p>
                <div
                  className={cx(
                    "my-2 p-1 flex border-2 bg-white rounded",
                    selectedItems && selectedItems.length > 0
                      ? " border-neutral-40"
                      : " border-neutral-20"
                  )}
                >
                  <div className="flex flex-auto flex-wrap">
                    {selectedItems &&
                      selectedItems.map((item, index) => {
                        return (
                          <div
                            key={index}
                            className="flex justify-center items-center m-1 font-medium py-1 px-2 bg-borderColor-secondary rounded-full "
                          >
                            <div className="text-xs font-normal leading-none max-w-full flex-initial">
                              {item.label}
                            </div>
                            <div className="flex flex-auto flex-row-reverse">
                              <button
                                className="justify-items-center ml-1 mb-1"
                                onClick={() => onClickRemoveItem(item.id)}
                              >
                                <Icon icon={remove} size={10} />
                              </button>
                            </div>
                          </div>
                        );
                      })}
                    <p className="text-center text-neutral-20 flex justify-center items-center px-2">
                      {placeholder}
                    </p>
                    <div className="flex-1">
                      <input
                        disabled
                        placeholder=""
                        className="cursor-pointer bg-transparent p-1 px-2 appearance-none outline-none h-full w-full text-gray-800"
                      />
                    </div>
                  </div>
                  <div
                    className="text-gray-300 w-8 py-1 pl-2 pr-1 border-l flex items-center border-gray-200"
                    onClick={toogleDropdown}
                  >
                    <button className="cursor-pointer w-6 h-6 text-gray-600 outline-none focus:outline-none">
                      {dropdown ? (
                        <Icon icon={angleUp} size={20} />
                      ) : (
                        <Icon icon={angleDown} size={20} />
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {dropdown ? (
              <Dropdown list={items} addItem={handleAddNewItem} />
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Multiselect;
