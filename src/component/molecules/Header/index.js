import Link from "next/link";
import { useRouter } from "next/router";
import styles from "./style.module.css";

function Component({ menuList }) {
  const router = useRouter();
  return (
    <header>
      <div className={styles.container}>
        <h1 className={styles.title}>Animeku</h1>
        <nav>
          <ul className={styles.navbar}>
            {menuList.map((item) => {
              return (
                <li id={item.id}>
                  <Link href={`/${item.slug}`}>
                    <a className="text-typhoColor-primary">{item.title}</a>
                  </Link>
                </li>
              );
            })}
          </ul>
        </nav>{" "}
      </div>
    </header>
  );
}

export default Component;
