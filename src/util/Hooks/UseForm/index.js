import { useEffect, useState } from "react";

export const useForm = (initialValue, validate, callback) => {
  const [values, setValues] = useState(initialValue);
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  const onSubmitForm = () => {
    setErrors(validate(values)); // for checking validation
    setIsSubmitting(true); // flagging after user sumbitted value
  };

  useEffect(() => {
    if (isSubmitting && Object.keys(errors).length > 0) {
      setIsSubmitting(false);
    } else if (
      isSubmitting &&
      Object.keys(errors).length === 0 &&
      errors.constructor === Object &&
      callback
    ) {
      // condition when user get error before submitted again
      callback(values, resetValue);
    }
  }, [isSubmitting, errors]);

  useEffect(() => {
    if (!isSubmitting && Object.keys(errors).length > 0) {
      // check after user submitted form but have error
      setErrors(validate(values));
    }
  }, [values]);

  const resetValue = () => {
    setValues(initialValue);
    setErrors({});
    setIsSubmitting(false);
  };

  return [
    values,
    (formType, formValue) => {
      if (formType === "reset") {
        return resetValue();
      } else if (formType === "form") {
        return setValues(formValue);
      }
      return setValues({ ...values, [formType]: formValue });
    },
    onSubmitForm,
    errors,
    isSubmitting,
    resetValue,
  ];
};
