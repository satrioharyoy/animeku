const withPlugins = require("next-compose-plugins");
const withImages = require("next-images");
const withFonts = require("next-fonts");

const nextConfig = {
  webpack5: true,
  swcMinify: true,
  images: {
    disableStaticImages: true,
    domains: ["s4.anilist.co"],
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
};

module.exports = withPlugins(
  [
    [
      {
        trailingSlash: true,
        optimizeImages: false,
        reactStrictMode: true,
      },
    ],
    withImages,
    withFonts,
  ],
  nextConfig
);
