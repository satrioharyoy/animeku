module.exports = {
  content: ["./public/**/*.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    screens: {
      xs: "375px",
      sm: "600px",
      md: "700px",
      lg: "1200px",
      xl: "1536px",
    },
    extend: {
      colors: {
        background: {
          primary: "#000000",
          secondary: "#131313",
          tertiary: "#ffc800",
        },
        typhoColor: {
          primary: "#ffc800",
          secondary: "#FFFFFF",
          tertiary: "#000000",
        },
        neutral: {
          10: "#FBFDFF",
          20: "#A3A5A8",
          40: "#505254",
          60: "#232425",
          80: "#202021",
          90: "#030D1B",
        },
        primary: {
          10: "#FDE8EB",
          20: "#FF8694",
          40: "#FF6D7F",
          60: "#FF5165",
          80: "#FF4A60",
          90: "#ED1A33",
        },
        secondary: {
          10: "#FFF7ED",
          20: "#FDD6A3",
          40: "#FCC67E",
          60: "#FBBD6C",
          80: "#FBB559",
          90: "#FAAD47",
        },
        success: {
          10: "#F1FBF8",
          20: "#81D112",
          40: "#66B30D",
          60: "#4F9609",
          80: "#4F9609",
          90: "#2B6403",
        },
        error: {
          10: "#FFF1F0",
          20: "#F0857D",
          40: "#FF5A4F",
          60: "#E02B1D",
          80: "#E01507",
          90: "#C43025",
        },
        borderColor: {
          primary: "#AEB1B4",
          secondary: "#F4F4F4",
        },
        disableColor: {
          primary: "#E9EEF3",
        },
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ["active"],
    },
  },
  plugins: [],
};
